package com.techu.back01.controllers;

import com.techu.back01.models.Producto;
import com.techu.back01.models.ProductoSoloPrecio;
import com.techu.back01.services.ProductosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductosController {

    @Autowired
    private ProductosService productosService;

    @GetMapping("")
    public String root() {
        return "Raiz del entregable BACK01";
    }

    //obtener todos los productos
    @GetMapping("/productos")
    public List<Producto> getProductos() {
        return productosService.getProductos();
    }

    //PASO 2. GET
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        Producto pr = productosService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    //PASO 3. POST
    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody Producto producto) {
        productosService.addProducto(producto);
        return new ResponseEntity<>("Producto creado satisfactoriamente!", HttpStatus.CREATED);
    }

    //PASO 4. PUT
    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody Producto productToUpdate) {
        Producto pr = productosService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productosService.updateProducto(id-1, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
        // Buenas prácticas dicen que mejor enviar 204 No Content ¿¿??
    }

    //PASO 5. DELETE
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        Producto pr = productosService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productosService.removeProducto(id - 1);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    //PASO 6. PATCH
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoSoloPrecio productoPrecioOnly, @PathVariable int id){
        Producto pr = productosService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productosService.updateProducto(id-1, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    //PASO 7. GET subrecurso
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        Producto pr = productosService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
