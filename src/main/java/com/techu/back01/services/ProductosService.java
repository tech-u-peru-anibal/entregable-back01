package com.techu.back01.services;

import com.techu.back01.models.Producto;
import com.techu.back01.models.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductosService {

    private List<Producto> dataList = new ArrayList<Producto>();

    public ProductosService() {
        dataList.add(new Producto(1, "producto 1", 100.50));
        dataList.add(new Producto(2, "producto 2", 150.00));
        dataList.add(new Producto(3, "producto 3", 100.00));
        dataList.add(new Producto(4, "producto 4", 50.75));
        dataList.add(new Producto(5, "producto 5", 120.00));
        List<User> users = new ArrayList<>();
        users.add(new User("1"));
        users.add(new User("3"));
        users.add(new User("5"));
        dataList.get(1).setUsers(users);
    }

    // READ Collections
    public List<Producto> getProductos() {
        return dataList;
    }

    // READ instance
    public Producto getProducto(long index) throws IndexOutOfBoundsException {
        if(getIndex(index)>=0) {
            return dataList.get(getIndex(index));
        }
        return null;
    }

    // CREATE
    public Producto addProducto(Producto newPro) {
        dataList.add(newPro);
        return newPro;
    }

    // UPDATE
    public Producto updateProducto(int index, Producto newPro)
            throws IndexOutOfBoundsException {
        dataList.set(index, newPro);
        return dataList.get(index);
    }

    // DELETE
    public void removeProducto(int index) throws IndexOutOfBoundsException {
        int pos = dataList.indexOf(dataList.get(index));
        dataList.remove(pos);
    }

    // Get index
    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<dataList.size()) {
            if(dataList.get(i).getId() == index){
                return(i); }
            i++;
        }
        return -1;
    }

}