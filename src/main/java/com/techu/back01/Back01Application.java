package com.techu.back01;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Back01Application {

	public static void main(String[] args) {
		SpringApplication.run(Back01Application.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunnerLog(ApplicationContext ctx)
	{
		return args -> {
			System.out.println("Back01 funcionando");
		};
	}

}
