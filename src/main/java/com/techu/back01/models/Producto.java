package com.techu.back01.models;

import java.util.List;
import java.util.Objects;

public class Producto {

    private long id;
    private String descripcion;
    private double precio;
    private List<User> users;

    public Producto() {
    }

    public Producto(long id, String descripcion, double precio) {
        this.setId(id);
        this.setDescripcion(descripcion);
        this.setPrecio(precio);
    }

    public Producto(long id, String descripcion, double precio, List<User> users) {
        this.setId(id);
        this.setDescripcion(descripcion);
        this.setPrecio(precio);
        this.setUsers(users);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductoModel{");
        sb.append("id=").append(id);
        sb.append(", descripcion='").append(descripcion).append('\'');
        sb.append(", precio=").append(precio);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Producto)) return false;
        Producto producto = (Producto) o;
        return getId() == producto.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
