package com.techu.back01.models;

public class User {
    private String userId;

    public User(){}

    public User(String userId){
        this.userId = userId;
    }

    public String getUserId(){
        return this.userId;
    }
}
