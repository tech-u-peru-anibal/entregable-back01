package com.techu.back01.models;

public class ProductoSoloPrecio {

    private long id;
    private double precio;

    public ProductoSoloPrecio() {
    }

    public ProductoSoloPrecio(double precio) {
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}

